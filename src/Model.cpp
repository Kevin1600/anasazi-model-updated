#include <stdio.h>
#include <cstdio>
#include <vector>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/Random.h"
#include "repast_hpc/Schedule.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/GridComponents.h"
#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/Edge.h"
#include "repast_hpc/SharedNetwork.h"

#include "Model.h"

//subtracts b<T> from a<T>
template <typename T>
void subtract_vector(std::vector<T>& a, const std::vector<T>& b)
{
	typename std::vector<T>::iterator       it = a.begin();
	typename std::vector<T>::const_iterator it2 = b.begin();

	while (it != a.end())
	{
		while (it2 != b.end() && it != a.end())
		{
			if (*it == *it2)
			{
				it = a.erase(it);
				it2 = b.begin();
			}

			else
				++it2;
		}
		if (it != a.end())
			++it;

		it2 = b.begin();
	}
}

AnasaziModel::AnasaziModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm) , locationContext(comm)
{
	props = new repast::Properties(propsFile, argc, argv, comm);
	boardSizeX = repast::strToInt(props->getProperty("board.size.x"));
	boardSizeY = repast::strToInt(props->getProperty("board.size.y"));

	initializeRandom(*props, comm);
	repast::Point<double> origin(0,0);
	repast::Point<double> extent(boardSizeX, boardSizeY);
	repast::GridDimensions gd (origin, extent);

	int procX = repast::strToInt(props->getProperty("proc.per.x"));
	int procY = repast::strToInt(props->getProperty("proc.per.y"));
	int bufferSize = repast::strToInt(props->getProperty("grid.buffer"));

	std::vector<int> processDims;
	processDims.push_back(procX);
	processDims.push_back(procY);
	householdSpace = new repast::SharedDiscreteSpace<Household, repast::StrictBorders, repast::SimpleAdder<Household> >("AgentDiscreteSpace",gd,processDims,bufferSize, comm);
	locationSpace = new repast::SharedDiscreteSpace<Location, repast::StrictBorders, repast::SimpleAdder<Location> >("LocationDiscreteSpace",gd,processDims,bufferSize, comm);

	edgeContentManager = new repast::RepastEdgeContentManager<Household>();
	householdNetwork = new repast::SharedNetwork<Household, repast::RepastEdge<Household>, repast::RepastEdgeContent<Household>, repast::RepastEdgeContentManager<Household> >("HouseholdKinshipNetwork", true, edgeContentManager);

	context.addProjection(householdSpace);
	locationContext.addProjection(locationSpace);
	context.addProjection(householdNetwork);

	param.startYear = repast::strToInt(props->getProperty("start.year"));
	param.endYear = repast::strToInt(props->getProperty("end.year"));
	param.maxStorageYear = repast::strToInt(props->getProperty("max.store.year"));
	param.maxStorage = repast::strToInt(props->getProperty("max.storage"));
	param.householdNeed = repast::strToInt(props->getProperty("household.need"));
	param.bufferLevel = repast::strToInt(props->getProperty("buffer.maize.level"));
	param.minFissionAge = repast::strToInt(props->getProperty("min.fission.age"));
	param.maxFissionAge = repast::strToInt(props->getProperty("max.fission.age"));
	param.minDeathAge = repast::strToInt(props->getProperty("min.death.age"));
	param.maxDeathAge = repast::strToInt(props->getProperty("max.death.age"));
	param.maxDistance = repast::strToInt(props->getProperty("max.distance"));
	param.initMinCorn = repast::strToInt(props->getProperty("initial.min.corn"));
	param.initMaxCorn = repast::strToInt(props->getProperty("initial.max.corn"));
	param.maxSpouseDistance = repast::strToInt(props->getProperty("max.spouse.distance"));

	param.annualVariance = repast::strToDouble(props->getProperty("annual.variance"));
	param.spatialVariance = repast::strToDouble(props->getProperty("spatial.variance"));
	param.fertilityProbability = repast::strToDouble(props->getProperty("fertility.prop"));
	param.harvestAdjustment = repast::strToDouble(props->getProperty("harvest.adj"));
	param.maizeStorageRatio = repast::strToDouble(props->getProperty("new.household.ini.maize"));

	year = param.startYear;
	stopAt = param.endYear - param.startYear + 1;
	fissionGen = new repast::DoubleUniformGenerator(repast::Random::instance()->createUniDoubleGenerator(0,1));
	deathAgeGen = new repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(param.minDeathAge,param.maxDeathAge));
	yieldGen = new repast::NormalGenerator(repast::Random::instance()->createNormalGenerator(0,param.annualVariance));
	soilGen = new repast::NormalGenerator(repast::Random::instance()->createNormalGenerator(0,param.spatialVariance));
	initAgeGen = new repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(0,param.minDeathAge));
	initMaizeGen = new repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(param.initMinCorn,param.initMaxCorn));

	string resultFile = props->getProperty("result.file");
	out.open(resultFile);
	out << "Year,Number-of-Households" << std::endl;

	string networkFile = props->getProperty("network.file");
	int l = networkFile.length();
	char ca[l+1];
	strcpy(ca, networkFile.c_str());
	out2 = fopen(ca, "w");
	fprintf(out2, "Year\tAdjacency Matrix\n");
}

AnasaziModel::~AnasaziModel()
{
	delete props;
	out.close();
	fclose(out2);
}

void AnasaziModel::initAgents()
{
	int rank = repast::RepastProcess::instance()->rank();

	int LocationID = 0;
	for(int i=0; i<boardSizeX; i++ )
	{
		for(int j=0; j<boardSizeY; j++)
		{
			repast::AgentId id(LocationID, rank, 1);
			Location* agent = new Location(id, soilGen->next());
			locationContext.addAgent(agent);
			locationSpace->moveTo(id, repast::Point<int>(i, j));
			LocationID++;
		}
	}

	readCsvMap();
	readCsvWater();
	readCsvPdsi();
	readCsvHydro();
	noOfAgents  = repast::strToInt(props->getProperty("count.of.agents"));
	repast::IntUniformGenerator xGen = repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(0,boardSizeX-1));
	repast::IntUniformGenerator yGen = repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(0,boardSizeY-1));
	for(int i =0; i< noOfAgents;i++)
	{
		repast::AgentId id(houseID, rank, 2);
		int initAge = initAgeGen->next();
		int mStorage = initMaizeGen->next();
		Household* agent = new Household(id, initAge, deathAgeGen->next(), mStorage);
		context.addAgent(agent);
		std::vector<Location*> locationList;

		newLocation:
		int x = xGen.next();
		int y = yGen.next();
		locationSpace->getObjectsAt(repast::Point<int>(x, y), locationList);

		if(locationList[0]->getState()==2)
		{
			locationList.clear();
			goto newLocation;
		}
		else
		{
			householdSpace->moveTo(id, repast::Point<int>(x, y));
			locationList[0]->setState(1);
			//std::cout << agent->getId() << x << y << agent->age <<std::endl;
			//spouseSearch(agent);
		}

		houseID++;
	}

	// std::vector< std::vector<int> > temp;
	// for(int i = 0; i < noOfAgents; ++i)
	// {
	// 	std::vector<int> v_temp;
	// 	for(int j = 0; j < noOfAgents; ++j)
	// 		v_temp.push_back(0);
	// 	temp.push_back(v_temp);
	// }
	std::vector< std::vector<int> > temp(noOfAgents, std::vector<int>(noOfAgents));
	a = temp;


	updateLocationProperties();

	repast::SharedContext<Household>::const_iterator local_agents_iter = context.begin();
	repast::SharedContext<Household>::const_iterator local_agents_end = context.end();

	while(local_agents_iter != local_agents_end)
	{
		Household* household = (&**local_agents_iter);
		if(household->death())
		{
			repast::AgentId id = household->getId();
			local_agents_iter++;

			std::vector<int> loc;
			householdSpace->getLocation(id, loc);

			std::vector<Location*> locationList;
			if(!loc.empty())
			{
				locationSpace->getObjectsAt(repast::Point<int>(loc[0], loc[1]), locationList);
				locationList[0]->setState(0);
			}
			context.removeAgent(id);
			for(int i = 0; i < a.size(); ++i)
				a[i][household->getId().id()] = -1;
			for(int j = 0; j < a[household->getId().id()].size(); ++j)
				a[household->getId().id()][j] = -1;
		}
		else
		{
			local_agents_iter++;
			fieldSearch(household);
		}
	}
}

void AnasaziModel::doPerTick()
{
	updateLocationProperties();
	writeOutputToFile();
	year++;
	updateHouseholdProperties();
}

void AnasaziModel::initSchedule(repast::ScheduleRunner& runner)
{
	runner.scheduleEvent(1, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<AnasaziModel> (this, &AnasaziModel::doPerTick)));
	runner.scheduleStop(stopAt);
}

void AnasaziModel::readCsvMap()
{
	int x,y,z , mz;
	string zone, maizeZone, temp;

	std::ifstream file ("data/map.csv");//define file object and open map.csv
	file.ignore(500,'\n');//Ignore first line

	while(1)//read until end of file
	{
		getline(file,temp,',');
		if(!temp.empty())
		{
			x = repast::strToInt(temp); //Read until ',' and convert to int & store in x
			getline(file,temp,',');
			y = repast::strToInt(temp); //Read until ',' and convert to int & store in y
			getline(file,temp,','); //colour
			getline(file,zone,',');// read until ',' and store into zone
			getline(file,maizeZone,'\n');// read until next line and store into maizeZone
			if(zone == "\"Empty\"")
			{
				z = 0;
			}
			else if(zone == "\"Natural\"")
			{
				z = 1;
			}
			else if(zone == "\"Kinbiko\"")
			{
				z = 2;
			}
			else if(zone == "\"Uplands\"")
			{
				z = 3;
			}
			else if(zone == "\"North\"")
			{
				z = 4;
			}
			else if(zone == "\"General\"")
			{
				z = 5;
			}
			else if(zone == "\"North Dunes\"")
			{
				z = 6;
			}
			else if(zone == "\"Mid Dunes\"")
			{
				z = 7;
			}
			else if(zone == "\"Mid\"")
			{
				z = 8;
			}
			else
			{
				z = 99;
			}

			if(maizeZone.find("Empty") != std::string::npos)
			{
				mz = 0;
			}
			else if(maizeZone.find("No_Yield") != std::string::npos)
			{
				mz = 1;
			}
			else if(maizeZone.find("Yield_1") != std::string::npos)
			{
				mz = 2;
			}
			else if(maizeZone.find("Yield_2") != std::string::npos)
			{
				mz = 3;
			}
			else if(maizeZone.find("Yield_3") != std::string::npos)
			{
				mz = 4;
			}
			else if(maizeZone.find("Sand_dune") != std::string::npos)
			{
				mz = 5;
			}
			else
			{
				mz = 99;
			}
			std::vector<Location*> locationList;
			locationSpace->getObjectsAt(repast::Point<int>(x, y), locationList);
			locationList[0]->setZones(z,mz);
		}
		else{
			goto endloop;
		}
	}
	endloop: ;
}

void AnasaziModel::readCsvWater()
{
	//read "type","start date","end date","x","y"
	int type, startYear, endYear, x, y;
	string temp;

	std::ifstream file ("data/water.csv");//define file object and open water.csv
	file.ignore(500,'\n');//Ignore first line
	while(1)//read until end of file
	{
		getline(file,temp,',');
		if(!temp.empty())
		{
			getline(file,temp,',');
			getline(file,temp,',');
			getline(file,temp,',');
			type = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			startYear = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			endYear = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			x = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,'\n');
			y = repast::strToInt(temp); //Read until ',' and convert to int

			std::vector<Location*> locationList;
			locationSpace->getObjectsAt(repast::Point<int>(x, y), locationList);
			locationList[0]->addWaterSource(type,startYear, endYear);
			//locationList[0]->checkWater(existStreams, existAlluvium, x, y, year);
		}
		else
		{
			goto endloop;
		}
	}
	endloop: ;
}

void AnasaziModel::readCsvPdsi()
{
	//read "year","general","north","mid","natural","upland","kinbiko"
	int i=0;
	string temp;

	std::ifstream file ("data/pdsi.csv");//define file object and open pdsi.csv
	file.ignore(500,'\n');//Ignore first line

	while(1)//read until end of file
	{
		getline(file,temp,',');
		if(!temp.empty())
		{
			pdsi[i].year = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			pdsi[i].pdsiGeneral = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			pdsi[i].pdsiNorth = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			pdsi[i].pdsiMid = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			pdsi[i].pdsiNatural = repast::strToDouble(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			pdsi[i].pdsiUpland = repast::strToDouble(temp); //Read until ',' and convert to int
			getline(file,temp,'\n');
			pdsi[i].pdsiKinbiko = repast::strToDouble(temp); //Read until ',' and convert to double
			i++;
		}
		else{
			goto endloop;
		}
	}
	endloop: ;
}

void AnasaziModel::readCsvHydro()
{
	//read "year","general","north","mid","natural","upland","kinbiko"
	string temp;
	int i =0;

	std::ifstream file ("data/hydro.csv");//define file object and open hydro.csv
	file.ignore(500,'\n');//Ignore first line

	while(1)//read until end of file
	{
		getline(file,temp,',');
		if(!temp.empty())
		{
			hydro[i].year = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			hydro[i].hydroGeneral = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			hydro[i].hydroNorth = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			hydro[i].hydroMid = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			hydro[i].hydroNatural = repast::strToDouble(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			hydro[i].hydroUpland = repast::strToDouble(temp); //Read until ',' and convert to int
			getline(file,temp,'\n');
			hydro[i].hydroKinbiko = repast::strToDouble(temp); //Read until ',' and convert to double
			i++;
		}
		else
		{
			goto endloop;
		}
	}
	endloop: ;
}

int AnasaziModel::yieldFromPdsi(int zone, int maizeZone)
{
	int pdsiValue, row, col;
	switch(zone)
	{
		case 1:
			pdsiValue = pdsi[year-param.startYear].pdsiNatural;
			break;
		case 2:
			pdsiValue = pdsi[year-param.startYear].pdsiKinbiko;
			break;
		case 3:
			pdsiValue = pdsi[year-param.startYear].pdsiUpland;
			break;
		case 4:
		case 6:
			pdsiValue = pdsi[year-param.startYear].pdsiNorth;
			break;
		case 5:
			pdsiValue = pdsi[year-param.startYear].pdsiGeneral;
			break;
		case 7:
		case 8:
			pdsiValue = pdsi[year-param.startYear].pdsiMid;
			break;
		default:
			return 0;
	}

	/* Rows of pdsi table*/
	if(pdsiValue < -3)
	{
		row = 0;
	}
	else if(pdsiValue >= -3 && pdsiValue < -1)
	{
		row = 1;
	}
	else if(pdsiValue >= -1 && pdsiValue < 1)
	{
		row = 2;
	}
	else if(pdsiValue >= 1 && pdsiValue < 3)
	{
		row = 3;
	}
	else if(pdsiValue >= 3)
	{
		row = 4;
	}
	else
	{
		return 0;
	}

	/* Col of pdsi table*/
	if(maizeZone >= 2)
	{
		col = maizeZone - 2;
	}
	else
	{
		return 0;
	}

	return yieldLevels[row][col];
}

double AnasaziModel::hydroLevel(int zone)
{
	switch(zone)
	{
		case 1:
			return hydro[year-param.startYear].hydroNatural;
		case 2:
			return hydro[year-param.startYear].hydroKinbiko;
		case 3:
			return hydro[year-param.startYear].hydroUpland;
		case 4:
		case 6:
			return hydro[year-param.startYear].hydroNorth;
		case 5:
			return hydro[year-param.startYear].hydroGeneral;
		case 7:
		case 8:
			return hydro[year-param.startYear].hydroMid;
		default:
			return 0;
	}
}

void AnasaziModel::checkWaterConditions()
{
	if ((year >= 280 && year < 360) or (year >= 800 && year < 930) or (year >= 1300 && year < 1450))
	{
		existStreams = true;
	}
	else
	{
		existStreams = false;
	}

	if (((year >= 420) && (year < 560)) or ((year >= 630) && (year < 680)) or	((year >= 980) && (year < 1120)) or ((year >= 1180) && (year < 1230)))
	{
		existAlluvium = true;
	}
	else
	{
		existAlluvium = false;
	}
}

void AnasaziModel::writeOutputToFile()
{
	out << year << "," <<  context.size() << std::endl;

	fprintf(out2, "%-4d\t    ", year);
	for(int i = 0; i < houseID; ++i)
		fprintf(out2, "%3d ", i);
	fprintf(out2, "\n     0  [");
	for(int i = 0; i < a.size(); ++i)
	{
		for(int j = 0; j < a[i].size(); ++j)
		{
			if(j < a[i].size()-1)
				fprintf(out2, "%3d ", a[i][j]);
			else if(j == a[i].size()-1 && i < a[i].size()-1)
				fprintf(out2, "%3d\n     %-3d ", a[i][j], i+1);
			else if(j == a[i].size()-1 && i == a[i].size()-1)
				fprintf(out2, "%3d]\n\n", a[i][j]);
		}
	}
}

void  AnasaziModel::updateLocationProperties()
{
	checkWaterConditions();
	int x = 0;
	for(int i=0; i<boardSizeX; i++ )
	{
		for(int j=0; j<boardSizeY; j++)
		{
			std::vector<Location*> locationList;
			locationSpace->getObjectsAt(repast::Point<int>(i, j), locationList);
			locationList[0]->checkWater(existStreams,existAlluvium, i, j, year);
			int mz = locationList[0]->getMaizeZone();
			int z = locationList[0]->getZone();
			int y = yieldFromPdsi(z,mz);
			locationList[0]->calculateYield(y, param.harvestAdjustment, yieldGen->next());
		}
	}
}

void AnasaziModel::updateHouseholdProperties()
{
	repast::SharedContext<Household>::const_iterator local_agents_iter = context.begin();
	repast::SharedContext<Household>::const_iterator local_agents_end = context.end();

	while(local_agents_iter != local_agents_end)
	{
		Household* household = (&**local_agents_iter);
		int currId = household->getId().id();
		if(household->death())
		{
			local_agents_iter++;
			removeHousehold(household);
			removedAgents.push_back(currId);

		}
		else
		{
			local_agents_iter++;
			if(household->fission(param.minFissionAge,param.maxFissionAge, fissionGen->next(), param.fertilityProbability))
			{
				spouseSearch(household);
			}

			if(household->checkState(param.householdNeed, param.bufferLevel)[0] != 2)
			{
				requestMaize(household);
			}

			bool fieldFound = true;
			if(household->checkState(param.householdNeed, param.bufferLevel)[0] == 0)
			{
				fieldFound = fieldSearch(household);
			}
			if(fieldFound)
			{
				household->nextYear(param.householdNeed);
			}
			else
			{
				removedAgents.push_back(currId);
			}
		}
	}
}

bool AnasaziModel::fieldSearch(Household* household)
{
	/******** Choose Field ********/
	std::vector<int> loc;
	householdSpace->getLocation(household->getId(), loc);

	std::vector<Location*> neighbouringLocations;
	std::vector<Location*> checkedLocations;
	repast::Moore2DGridQuery<Location> moore2DQuery(locationSpace);
	int range = 1;
	while(1)
	{
		moore2DQuery.query(loc, range, false, neighbouringLocations);

		for (std::vector<Location*>::iterator it = neighbouringLocations.begin() ; it != neighbouringLocations.end(); ++it)
		{
			Location* tempLoc = (&**it);
			if(tempLoc->getState() == 0)
			{
				if(tempLoc->getExpectedYield() >= param.householdNeed)
				{
					std::vector<int> loc;
					locationSpace->getLocation(tempLoc->getId(), loc);
					tempLoc->setState(2);
					household->chooseField(tempLoc);
					goto EndOfLoop;
				}
			}
		}
		range++;
		if(range > boardSizeY)
		{
			removeHousehold(household);
			return false;
		}
	}
	EndOfLoop:
	if(range >= 10)
	{
		return relocateHousehold(household);
	}
	else
	{
		return true;
	}
}

void AnasaziModel::removeHousehold(Household* household)
{
	repast::AgentId id = household->getId();

	std::vector<int> loc;
	householdSpace->getLocation(id, loc);

	std::vector<Location*> locationList;
	std::vector<Household*> householdList;
	if(!loc.empty())
	{
		locationSpace->getObjectsAt(repast::Point<int>(loc[0], loc[1]), locationList);
		householdSpace->getObjectsAt(repast::Point<int>(loc[0], loc[1]), householdList);
		if(householdList.size() == 1)
		{
			locationList[0]->setState(0);
		}
		if(household->getAssignedField()!= NULL)
		{
			std::vector<int> loc;
			locationSpace->getLocation(household->getAssignedField()->getId(), loc);
			locationSpace->getObjectsAt(repast::Point<int>(loc[0], loc[1]), locationList);
			locationList[0]->setState(0);
		}
	}

	for(int i = 0; i < a.size(); ++i)
		a[i][household->getId().id()] = -1;
	for(int j = 0; j < a[household->getId().id()].size(); ++j)
		a[household->getId().id()][j] = -1;

	context.removeAgent(id);
}

bool AnasaziModel::relocateHousehold(Household* household)
{
	std::vector<Location*> neighbouringLocations;
	std::vector<Location*> suitableLocations;
	std::vector<Location*> waterSources;
	std::vector<Location*> checkedLocations;

	std::vector<int> loc, loc2;
	locationSpace->getLocation(household->getAssignedField()->getId(), loc);
	householdSpace->getLocation(household->getId(),loc2);

	locationSpace->getObjectsAt(repast::Point<int>(loc2[0], loc2[1]), neighbouringLocations);
	Location* householdLocation = neighbouringLocations[0];

	repast::Moore2DGridQuery<Location> moore2DQuery(locationSpace);
	int range = floor(param.maxDistance/100);
	int i = 1;
	bool conditionC = true;

	//get all !Field with 1km
	LocationSearch:
		moore2DQuery.query(loc, range*i, false, neighbouringLocations);
		for (std::vector<Location*>::iterator it = neighbouringLocations.begin() ; it != neighbouringLocations.end(); ++it)
		{
			Location* tempLoc = (&**it);
			if(tempLoc->getState() != 2)
			{
				if(householdLocation->getExpectedYield() < tempLoc->getExpectedYield() && conditionC == true)
				{
					suitableLocations.push_back(tempLoc);
				}
				if(tempLoc->getWater())
				{
					waterSources.push_back(tempLoc);
				}
			}
		}
		if(suitableLocations.size() == 0 || waterSources.size() == 0)
		{
			if(conditionC == true)
			{
				conditionC = false;
			}
			else
			{
				conditionC = true;
				i++;
				if(range*i > boardSizeY)
				{
					removeHousehold(household);
					return false;
				}
			}
			goto LocationSearch;
		}
		else if(suitableLocations.size() == 1)
		{
			std::vector<int> loc2;
			locationSpace->getLocation(suitableLocations[0]->getId(),loc2);
			householdSpace->moveTo(household->getId(),repast::Point<int>(loc2[0], loc2[1]));
			return true;
		}
		else
		{
			std::vector<int> point1, point2;
			std::vector<double> distances;
			for (std::vector<Location*>::iterator it1 = suitableLocations.begin() ; it1 != suitableLocations.end(); ++it1)
			{
				locationSpace->getLocation((&**it1)->getId(),point1);
				for (std::vector<Location*>::iterator it2 = waterSources.begin() ; it2 != waterSources.end(); ++it2)
				{
					locationSpace->getLocation((&**it2)->getId(),point2);
					double distance = sqrt(pow((point1[0]-point2[0]),2) + pow((point1[1]-point2[1]),2));
					distances.push_back(distance);
				}
			}
			int minElementIndex = std::min_element(distances.begin(),distances.end()) - distances.begin();
			minElementIndex = minElementIndex / waterSources.size();
			std::vector<int> loc2;
			locationSpace->getLocation(suitableLocations[minElementIndex]->getId(),loc2);
			householdSpace->moveTo(household->getId(),repast::Point<int>(loc2[0], loc2[1]));
			return true;
		}
}

void AnasaziModel::spouseSearch(Household* household)
{
	std::vector<int> loc;
	householdSpace->getLocation(household->getId(),loc);

	std::vector<Household*> neighbouringHouseholds;
	std::vector<Household*> neighbouringHouseholdsPrev;
	std::vector<Household*> temp;
	std::vector<Household*> suitableSpouseHouseholds;
	Household* spouseHousehold;
	bool spouseFound = false;
	int maxRadius = floor(param.maxSpouseDistance/100);

	repast::Moore2DGridQuery<Household> moore2DQuery(householdSpace);

	for(int searchRadius = 0; searchRadius <= maxRadius; searchRadius++)
	{
		moore2DQuery.query(loc, searchRadius, true, neighbouringHouseholds);
		temp = neighbouringHouseholds;
		subtract_vector(neighbouringHouseholds, neighbouringHouseholdsPrev);
		neighbouringHouseholdsPrev = temp;

		for(std::vector<Household*>::iterator it = neighbouringHouseholds.begin() ; it != neighbouringHouseholds.end(); ++it)
		{
			if((**it).getId() != household->getId())
			{
				//std::cout << (**it).getId() << std::endl;
				if((**it).fertile(param.minFissionAge, param.maxFissionAge))
				{
					if((!householdNetwork->findEdge(household, &**it) && !householdNetwork->findEdge(&**it, household)) || 
						((householdNetwork->findEdge(household, &**it)) && (householdNetwork->findEdge(household, &**it)->weight() == kinType.spouse)))
					{
						suitableSpouseHouseholds.push_back(&**it);
					}
				}
			}
		}
		if(suitableSpouseHouseholds.size() == 1)
		{
			spouseHousehold = suitableSpouseHouseholds[0];
			householdNetwork->addEdge(household, spouseHousehold, kinType.spouse);
			householdNetwork->addEdge(spouseHousehold, household, kinType.spouse);
			a[household->getId().id()][spouseHousehold->getId().id()] = kinType.spouse;
			a[spouseHousehold->getId().id()][household->getId().id()] = kinType.spouse;
			//std::cout << spouseHousehold->getId() << std::endl;
			spouseFound = true;
			goto endloop;
		}
		else if(suitableSpouseHouseholds.size() > 1)
		{
			repast::IntUniformGenerator* randomIntGen = new repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(0,suitableSpouseHouseholds.size()-1));
			int i = randomIntGen->next();
			spouseHousehold = suitableSpouseHouseholds[i];
			householdNetwork->addEdge(household, spouseHousehold, kinType.spouse);
			householdNetwork->addEdge(spouseHousehold, household, kinType.spouse);
			a[household->getId().id()][spouseHousehold->getId().id()] = kinType.spouse;
			a[spouseHousehold->getId().id()][household->getId().id()] = kinType.spouse;
			//std::cout << spouseHousehold->getId() << std::endl;
			spouseFound = true;
			goto endloop;
		}
	}

	endloop:
	if(spouseFound)
	{
		//std::cout << household->getId() << year << std::endl;
		int rank = repast::RepastProcess::instance()->rank();
		repast::AgentId id(houseID, rank, 2);
		int mStorage = household->splitMaizeStored(param.maizeStorageRatio);
		Household* newAgent = new Household(id, 0, deathAgeGen->next(), mStorage);
		context.addAgent(newAgent);

		// create new row in adjacency matrix a
		std::vector<int> v_temp(houseID+1);
		a.push_back(v_temp);

		// create new column in adjacency matrix a
		for(int i = 0; i < a.size()-1; ++i)
		{
			std::vector<int>::iterator v_it = std::find(removedAgents.begin(), removedAgents.end(), i);
			if(v_it != removedAgents.end())
				a[i].push_back(-1);
			else
				a[i].push_back(0);
		}

		// set elements of new row that correspond to removed agents to -1
		for(int j = 0; j < a.size()-1; ++j)
		{
			std::vector<int>::iterator v_it = std::find(removedAgents.begin(), removedAgents.end(), j);
			if(v_it != removedAgents.end())
				a[houseID][j] = -1;
		}

		std::vector<Household*> predecessors_;
		householdNetwork->predecessors(household, predecessors_);
		for(std::vector<Household*>::iterator it = predecessors_.begin() ; it != predecessors_.end(); ++it)
		{
			if(householdNetwork->findEdge(&**it, household)->weight() == kinType.parent_child)
			{
				householdNetwork->addEdge(&**it, newAgent, kinType.grandparent_grandchild);
				a[newAgent->getId().id()][(**it).getId().id()] = kinType.grandparent_grandchild;
			}
		}

		std::vector<Household*> successors_;
		householdNetwork->successors(household, successors_);
		for(std::vector<Household*>::iterator it = successors_.begin() ; it != successors_.end(); ++it)
		{
			if(householdNetwork->findEdge(household, &**it)->weight() == kinType.parent_child)
			{
				householdNetwork->addEdge(&**it, newAgent, kinType.sibling);
				householdNetwork->addEdge(newAgent, &**it, kinType.sibling);
				a[newAgent->getId().id()][(**it).getId().id()] = kinType.sibling;
				a[(**it).getId().id()][newAgent->getId().id()] = kinType.sibling;
			}
		}

		householdNetwork->addEdge(household, newAgent, kinType.parent_child);
		a[newAgent->getId().id()][household->getId().id()] = kinType.parent_child;

		std::vector<Household*> spousePredecessors;
		householdNetwork->predecessors(spouseHousehold, spousePredecessors);
		for(std::vector<Household*>::iterator it2 = spousePredecessors.begin() ; it2 != spousePredecessors.end(); ++it2)
		{
			if(householdNetwork->findEdge(&**it2, spouseHousehold)->weight() == kinType.parent_child)
			{
				householdNetwork->addEdge(&**it2, newAgent, kinType.grandparent_grandchild);
				a[newAgent->getId().id()][(**it2).getId().id()] = kinType.grandparent_grandchild;
			}
		}
		std::vector<Household*> spouseSuccessors;
		householdNetwork->successors(spouseHousehold, spouseSuccessors);
		for(std::vector<Household*>::iterator it3 = spouseSuccessors.begin() ; it3 != spouseSuccessors.end(); ++it3)
		{
			if(householdNetwork->findEdge(spouseHousehold, &**it3)->weight() == kinType.parent_child)
			{
				householdNetwork->addEdge(&**it3, newAgent, kinType.sibling);
				householdNetwork->addEdge(newAgent, &**it3, kinType.sibling);
				a[newAgent->getId().id()][(**it3).getId().id()] = kinType.sibling;
				a[(**it3).getId().id()][newAgent->getId().id()] = kinType.sibling;
			}
		}
		householdNetwork->addEdge(spouseHousehold, newAgent, kinType.parent_child);
		a[newAgent->getId().id()][spouseHousehold->getId().id()] = kinType.parent_child;

		householdSpace->moveTo(id, repast::Point<int>(loc[0], loc[1]));
		fieldSearch(newAgent);
		houseID++;
	}

}

void AnasaziModel::requestMaize(Household* household)
{
	if(household->checkState(param.householdNeed, param.bufferLevel)[0] == 1)
	{
		std::vector<Household*> adjacents;
		householdNetwork->adjacent(household, adjacents);
		for(std::vector<Household*>::iterator it = adjacents.begin() ; it != adjacents.end(); ++it)
		{
			int* alterState = (**it).checkState(param.householdNeed, param.bufferLevel);
			if(alterState[0] == 2)
			{
				int maizeAmount = std::min(household->checkState(param.householdNeed, param.bufferLevel)[1], alterState[1]);
				if(maizeAmount != 0)
				{
					(**it).takeMaize(maizeAmount);
					household->giveMaize(maizeAmount);
					//std::cout << (**it).getId() << household->getId() << year << maizeAmount << std::endl;
					if(household->checkState(param.householdNeed, param.bufferLevel)[0] == 2)
						return;
				}
			}
		}
	}
	else if(household->checkState(param.householdNeed, param.bufferLevel)[0] == 0)
	{
		std::vector<Household*> adjacents;
		householdNetwork->adjacent(household, adjacents);
		for(std::vector<Household*>::iterator it = adjacents.begin() ; it != adjacents.end(); ++it)
		{
			int* alterState = (**it).checkState(param.householdNeed, param.bufferLevel);
			if(alterState[0] == 2)
			{
				int maizeAmount = std::min(household->checkState(param.householdNeed, param.bufferLevel)[1], alterState[1] + (param.bufferLevel - param.householdNeed));
				if(maizeAmount != 0)
				{
					(**it).takeMaize(maizeAmount);
					household->giveMaize(maizeAmount);
					//std::cout << (**it).getId() << household->getId() << year << maizeAmount << std::endl;
					if(household->checkState(param.householdNeed, param.bufferLevel)[0] == 2)
						return;
				}
			}
			else if(alterState[0] = 1)
			{
				int maizeAmount = std::min(household->checkState(param.householdNeed, param.bufferLevel)[1], (param.bufferLevel - param.householdNeed) - alterState[1]);
				if(maizeAmount != 0)
				{
					(**it).takeMaize(maizeAmount);
					household->giveMaize(maizeAmount);
					//std::cout << (**it).getId() << household->getId() << year << maizeAmount << std::endl;
					if(household->checkState(param.householdNeed, param.bufferLevel)[0] == 2)
						return;
				}
			}
		}
	}
}
