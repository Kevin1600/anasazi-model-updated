#include "Household.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include <stdio.h>
#include "repast_hpc/Random.h"

Household::Household(repast::AgentId id, int a, int deAge, int mStorage)
{
	householdId = id;
	age = a;
	deathAge = deAge;
	maizeStorage = mStorage;
	assignedField = NULL;
}

Household::~Household()
{

}

int Household::splitMaizeStored(int percentage)
{
	int maizeEndowment;
	maizeEndowment = maizeStorage * percentage;
	maizeStorage = maizeStorage - maizeEndowment;
	return maizeEndowment;
}

int* Household::checkState(int needs, int bufferLevel)
{
	int totalYield = assignedField->getExpectedYield() + maizeStorage;
	static int state[2];
	if( totalYield >= needs && totalYield < bufferLevel)
	{
		state[0] = 1; //"Hungry" state
		state[1] = bufferLevel - totalYield;
		return state;
	}
	else if(totalYield < needs)
	{
		state[0] = 0; //"Starving" state
		state[1] = needs - totalYield;
		return state;
	}
	else
	{
		state[0] = 2; //"Satisfied" state
		state[1] = totalYield - bufferLevel;
		return state;
	}
}

void Household::takeMaize(int mAmount)
{
	maizeStorage = maizeStorage - mAmount;
}

void Household::giveMaize(int mAmount)
{
	maizeStorage = maizeStorage + mAmount;
}

bool Household::death()
{
	if(age>=deathAge)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Household::fission(int minFissionAge, int maxFissionAge, double gen, double fProb)
{
	if((age>=minFissionAge && age<=maxFissionAge) && (gen <= fProb))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Household::fertile(int minFissionAge, int maxFissionAge)
{
	if(age>=minFissionAge && age<=maxFissionAge) 
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Household::nextYear(int needs)
{
	age++;
	maizeStorage = assignedField->getExpectedYield() + maizeStorage - needs;
}

void Household::chooseField(Location* Field)
{
	assignedField = Field;
}
